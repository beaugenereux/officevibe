from bs4 import BeautifulSoup
from selenium import webdriver

import time

driver = webdriver.Chrome()

driver.get('https://www.softwareadvice.com/hr/officevibe-profile/reviews/')

# Quick pause so the page fully loads before we scrape.
time.sleep(5)

# Let's grab the page source so we leave their servers alone.
page_html = driver.page_source

soup = BeautifulSoup(page_html, 'html.parser')
divs = soup.find_all('div', {'class':'review'})
#for div in divs:
#    print(div.prettify())

print(divs)
# # Save the sites locally so we don't stress the servers.
# def parse(self, response):
#     # Split url before its third slash from the end.
#     page = response.url.split("/")[-2]
#     # Name file after split.
#     filename = 'capterra-%s.html' % page
#     with open(filename, 'wb') as file:
#         file.write(response.body)
#     print('Saved file %s' % filename)
