import os


def readFile(filename):
    filehandle = open(filename)
    print filehandle.read()
    filehandle.close()



fileDir = os.path.dirname(os.path.realpath('__file__'))
print fileDir

#For accessing the file in the same folder
filename = "same.txt"
readFile(filename)

#For accessing the file in a folder contained in the current folder
filename = os.path.join(fileDir, 'Folder1.1/same.txt')
readFile(filename)

#For accessing the file in the parent folder of the current folder
filename = os.path.join(fileDir, '../same.txt')
readFile(filename)

#For accessing the file inside a sibling folder.
filename = os.path.join(fileDir, '../Folder2/same.txt')
filename = os.path.abspath(os.path.realpath(filename))
print filename
readFile(filename)


# https://stackoverflow.com/questions/7165749/open-file-in-a-relative-location-in-python

# import os
# script_path = os.path.abspath(__file__) # i.e. /path/to/dir/foobar.py
# script_dir = os.path.split(script_path)[0] #i.e. /path/to/dir/
# rel_path = "2091/data.txt"
# abs_file_path = os.path.join(script_dir, rel_path)
#
# ## THEN this came up
#
# from os import path
#
# file_path = path.relpath("2091/data.txt")
# with open(file_path) as f:
#     <do stuff>
#
# # Simpler code
# with open("2091/data/txt") as f:
#     <do stuff>
