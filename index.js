const selenium = require('selenium-webdriver');
const By = selenium.By;

const HomePage = require('./pages/home.js');

const driver = new selenium.Builder().forBrowser('chrome').build();

// Create a page object out of the url sent.
const homePage = new HomePage(driver);
homePage.open();
//const url = "https://www.softwareadvice.com/hr/officevibe-profile/reviews/";

//driver.get(url);
