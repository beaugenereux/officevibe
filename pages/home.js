const selenium = require('selenium-webdriver');
const By = selenium.By;

class HomePage {
    // Initialize page object model.
    constructor(driver) {
        this.driver = driver;
        this.locators = {

        };
    }

    // Open the url you were given.
    open() {
        // We'll launch this app with
        // $ URL=https://www.g2.com/products/officevibe/reviews?utf8=%E2%9C%93&order=most_recent&filters%5Bcomment_answer_values%5D= node index.js
        this.driver.get(process.env.URL);
    }

    // XPath for one review: //*[@id="survey-response-4229924"]
}

// If you import this file (home.js), you can export HomePage.
module.exports = HomePage;
