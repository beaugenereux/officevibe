from os import path
from bs4 import BeautifulSoup
import csv

import re

file_path = path.relpath("g2/reviews?order=most_recent.html")
with open(file_path) as file:
    soup = BeautifulSoup(file, 'html.parser')
    g2reviews = soup.find_all('div', {'itemprop':'review'})

    # Create a file to write to, add headers row
    writer = csv.writer(open('g2reviews.csv', 'w'))
    writer.writerow(['Date', 'Title', 'Feedback'])

    for review in g2reviews:
        date = review.find('time').contents[0]
        title = review.find('h3', {'itemprop':'name'}).contents[0]
        feedback = review.find_all('p', {'class':'formatted-text'})

        author = review.find_all('div', {'class':'mt-4th'})
        # reviewer = review.find('div', {'class':'mt-4th'}).previous_sibling
        # enterprise = review.find('div', {'class':'mt-4th'}).next_sibling

        # For each <p> found, delete the <span>
        for blurp in feedback:
            for s in blurp.select('span'):
                s.decompose() # Delete that span please.
            blurp.attrs = {} # This removed the class but not the tags.

        # Print to test results.
        print(date,'\n', title,'\n', feedback, '\n*****\n')

        # Save into .csv file specified earlier.
        writer.writerow([date, title, feedback])

    # TODO:
    # DONE- Remove the span tag
    # DONE- Save exported reviews into a .csv or GoogleSheet
    # -- Remove other html tags but not their content
    # DONE- Save the date of the review
    # -- Save the enterprise size of the review's author
    # That proved harder than expected. Tabled for now, gotta move on.
