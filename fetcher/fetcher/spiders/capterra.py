import scrapy


class CapterraSpider(scrapy.Spider):
    name = "capterra"

    # List the sites to scrape and save locally.
    def start_requests(self):
        urls = [
            'https://www.capterra.com/p/132908/Officevibe/reviews/',
        ]
        for url in urls:
            print('*****\n', url)
            yield scrapy.Request(url=url, callback=self.parse)

    # Save the sites locally so we don't stress the servers.
    def parse(self, response):
        # Split url before its third slash from the end.
        page = response.url.split("/")[-2]
        # Name file after split.
        filename = 'capterra-%s.html' % page
        with open(filename, 'wb') as file:
            file.write(response.body)
        print('Saved file %s' % filename)
