import scrapy


class FetchSpider(scrapy.Spider):
    name = "g2"

    # List the sites to scrape and save locally.
    def start_requests(self):
        urls = [
            # 'https://www.g2.com/products/officevibe/reviews',
            # 'https://www.g2.com/products/officevibe/reviews?page=2',
            # 'https://www.g2.com/products/officevibe/reviews?page=3',
            # 'https://www.g2.com/products/officevibe/reviews?page=4',
            # 'https://www.g2.com/products/officevibe/reviews?page=5',
            # 'https://www.g2.com/products/officevibe/reviews?page=6',
            # 'https://www.g2.com/products/officevibe/reviews?page=7',
            # 'https://www.g2.com/products/officevibe/reviews?page=8',
            # 'https://www.g2.com/products/officevibe/reviews?page=9',
            # 'https://www.g2.com/products/officevibe/reviews?page=10',
            # 'https://www.g2.com/products/officevibe/reviews?page=11',
            # 'https://www.g2.com/products/officevibe/reviews?page=12',
            # 'https://www.g2.com/products/officevibe/reviews?page=13',
            # 'https://www.g2.com/products/officevibe/reviews?page=14',
            # 'https://www.g2.com/products/officevibe/reviews?page=15',
            # 'https://www.g2.com/products/officevibe/reviews?page=16',
            # 'https://www.g2.com/products/officevibe/reviews?page=17',
            # 'https://www.g2.com/products/officevibe/reviews?page=18',
            # 'https://www.g2.com/products/officevibe/reviews?page=19',
            # 'https://www.g2.com/products/officevibe/reviews?page=20',
            # 'https://www.g2.com/products/officevibe/reviews?page=21',
            # 'https://www.g2.com/products/officevibe/reviews?page=22',
            # 'https://www.g2.com/products/officevibe/reviews?page=23',
            # 'https://www.g2.com/products/officevibe/reviews?page=24',
            # 'https://www.g2.com/products/officevibe/reviews?page=25',
            # 'https://www.g2.com/products/officevibe/reviews?page=26',
            'https://www.g2.com/products/officevibe/reviews?utf8=%E2%9C%93&order=most_recent&filters%5Bcomment_answer_values%5D=',
        ]
        for url in urls:
            print('*****\n', url)
            yield scrapy.Request(url=url, callback=self.parse)

    # Save the sites locally so we don't stress the servers.
    def parse(self, response):
        # Split url before its third slash from the end.
        page = response.url.split("/")[-1]
        # Name file after split.
        filename = '%s.html' % page
        with open(filename, 'wb') as file:
            file.write(response.body)
        print('Saved file %s' % filename)
